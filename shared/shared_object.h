#pragma once
#include <vec3f.h>

class shared_object
{
public:
	shared_object();
	void init_pointers(vec3f position, vec3f rotation, vec3f scale)
	{
		this->position_ = position_;
		this->rotation_ = rotation_;
		this->scale_ = scale_;
	}
	void on_create()
	{

	}
	void on_update()
	{
		
	}
	void on_draw()
	{
		
	}
	void setXYZPosition(float x, float y, float z);
	void setXYZRotation(float x, float y, float z);
	void setXYZScale(float x, float y, float z);
	vec3f getXYZPosition();
	vec3f getXYZRotation();
	vec3f getXYZScale();
protected:
private:
	vec3f position_;
	vec3f rotation_;
	vec3f scale_;
};