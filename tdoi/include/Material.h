#ifndef MATERIAL_H
#define MATERIAL_H

#include <Program.h>
#include <Texture.h>

// Include GLM
#include <glm/glm.hpp>

class Material
{
    public:
        Material();
        Material(unsigned char surface_type, Program* shader_program, Texture* texture);
        virtual ~Material();

        void setSurfaceType(unsigned char surface_type);
        void setShaderProgram(Program* program, bool delete_old);
        void setTexture(Texture* texture, bool delete_old);
        void useMaterial(glm::mat4 mvp);

        unsigned char getSurfaceType();
        Program* getShaderProgram();
        Texture* getTexture();
        void Delete();
        bool isDeleted();

    protected:

    private:
        bool m_deleted;
        unsigned char m_surfaceType;
        Program* m_pShader;
        Texture* m_pTexture;
};

#endif // MATERIAL_H
