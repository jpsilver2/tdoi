#pragma once
#include "GuiBase.h"
#include "iamGui\Addons\imguifilesystem\imguifilesystem.h"

class CreateMaterialWindow :
	public GuiBase
{
public:
	CreateMaterialWindow();
	CreateMaterialWindow(ResourceHandler* rh, EntityHandler* eh);
	void Render() override;
private:
	int m_surface;
	char m_material_name_[32];
	char m_texture_name_[32];
	char m_shader_name_[32];
	unsigned char error_code_ = -1;
};

