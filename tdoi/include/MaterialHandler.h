#ifndef MATERIALHANDLER_H
#define MATERIALHANDLER_H

#include <vector>
#include <Material.h>


class MaterialHandler
{
    public:
        struct MaterialInfo{
            Material* material;
            const char* name;
        };
        MaterialHandler();
        virtual ~MaterialHandler();
        Material* addMaterial(unsigned char surface_type, Program* shader, Texture* texture, const char* material_name);
        Material* getMaterial(const char* material_name);
        void useMaterial(const char* material_name, glm::mat4 mvp);
        void cleanMaterials();
	unsigned int getNumberOfMaterials();
	vector<const char*> getMaterialNames();

protected:

    private:
        std::vector<MaterialInfo> m_deletedMaterials;
        std::vector<MaterialInfo> m_materialBatch;
};

#endif // MATERIALHANDLER_H
