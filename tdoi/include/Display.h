#ifndef DISPLAY_H
#define DISPLAY_H

#include <string>
// Include standard headers
#include <stdio.h>
#include <stdlib.h>
// Include GLEW. Always include it before gl.h and glfw3.h, since it's a bit magic.
#include <GL/glew.h>
// Include GLFW
#include <GLFW/glfw3.h>
// Include GLM
#include <glm/glm.hpp>
#include "glm/gtc/matrix_transform.hpp"
#include <EntityHandler.h>
#include <DisplaySettings.h>
#include <ResourceHandler.h>
#include <ModelLoader.h>
#include <string>
#include <Camera.h>
#include <WindowHandler.h>
#include "CreateEntityWindow.h"
#include "CreateMaterialWindow.h"
#include "CreateTextureWindow.h"
#include "CreateShaderWindow.h"

class Display
{
    public:
        Display(DisplaySettings settings);
        virtual ~Display();
        bool hasError();
        void gameLoop();

    protected:
        void initializeWindow();
        void createWindow();
        glm::mat4 getProjectionMatrix();

    private:
        DisplaySettings m_settings;
        GLFWwindow* m_pWindow = nullptr;
        unsigned int m_errorCode = -1;
        void enableGLConstants();
        void clean();
        void load();
        void init();
		void handleMouse();
		void handleInput();
		void input();
		void update();
		void draw();
		double beginRender();
		void endRender(double startTime);
		bool isRunning = true;
};

#endif // DISPLAY_H
