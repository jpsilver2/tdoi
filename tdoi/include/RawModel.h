#pragma once
#include <VertexArray.h>
#include <VertexBuffer.h>
class RawModel
{
public:
	RawModel();
	RawModel(unsigned int num_buffers, std::initializer_list<VertexBuffer*> args);
	void draw();
private:
	VertexArray m_vertex_array_;
};