#ifndef CAMERA_H
#define CAMERA_H

// Include GLEW. Always include it before gl.h and glfw3.h, since it's a bit magic.
#include <GL/glew.h>
// Include GLFW
#include <GLFW/glfw3.h>
// Include GLM
#include <glm/glm.hpp>
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include <math.h>       /* cos */


class Camera
{
    public:
        Camera();
        virtual ~Camera();
        void update(double deltaTime, double mx, double my);
        glm::mat4 getCameraMatrix();
        void moveForward(double deltaTime);
        void moveBackwards(double deltaTime);
        void moveLeft(double deltaTime);
        void moveRight(double deltaTime);
		void translate(glm::vec3 translation);
		float getSpeed();

    protected:

    private:
        glm::vec3 m_position = glm::vec3(0,0,0);
        float horizontalAngle = 0.0F;
        float verticalAngle = 0.0F;
        float speed = 30;
        float mouseSpeed = 1.0F;
        glm::vec3 direction = glm::vec3(0, 0, 0);
        glm::vec3 up;
        glm::vec3 right;
};

#endif // CAMERA_H
