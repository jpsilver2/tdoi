#pragma once
#include <shared_object.h>
#include <RawModel.h>
#include <Material.h>
#include <vec3f.h>
#include <glm/glm.hpp>
#include "glm/gtc/matrix_transform.hpp"

class object
{
public:
	object(Material* material, RawModel* raw_model, shared_object *shared_object);
	~object();
	void init_pointers(vec3f position, vec3f rotation, vec3f scale);
	void on_create();
	void on_update();
	void on_draw();
	void useMaterial(glm::mat4 mvp);
	void draw();
private:
	shared_object * m_p_shared_object_;
	Material* m_pMaterial;
	RawModel* m_p_raw_model;
};

