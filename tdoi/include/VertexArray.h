#ifndef VERTEXARRAY_H
#define VERTEXARRAY_H

// Include GLEW. Always include it before gl.h and glfw3.h, since it's a bit magic.
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

#include <DisplaySettings.h>

// Include GLM(Math library)
#include <glm/glm.hpp>

#include <VertexBuffer.h>
#include <iostream>

class VertexArray
{
    public:
        VertexArray();
        VertexArray(VertexBuffer *vertexBuffer);
        VertexArray(VertexBuffer **vertexBuffer, unsigned int num_buffers);
        VertexArray(unsigned int size,  std::initializer_list<VertexBuffer*> args);
        virtual ~VertexArray();
        void bind();
        void draw();

    protected:

    private:
        unsigned int m_id;
        unsigned int m_bufferCount;
        VertexBuffer **m_pVertexBuffer;
};

#endif // VERTEXARRAY_H
