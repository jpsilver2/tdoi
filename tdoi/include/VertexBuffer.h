#ifndef VERTEXBUFFER_H
#define VERTEXBUFFER_H
// Include GLEW. Always include it before gl.h and glfw3.h, since it's a bit magic.
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

#include <GL/gl.h>

#include <iostream>

#include <vector>

// Include GLM
#include <glm/glm.hpp>

class VertexBuffer
{
    public:
        VertexBuffer();
        VertexBuffer(float *data, unsigned int data_cout, unsigned int attrib);
        VertexBuffer(std::vector<glm::vec3> data, unsigned int attrib);
        VertexBuffer(std::vector<glm::vec2> data, unsigned int attrib);
        virtual ~VertexBuffer();
        void bind();
        void unbind();
        unsigned int getVerticeCount();
        void enableAttribArray();
        void disableAttribArray();
        void setDataSize(unsigned int size);

    protected:

    private:
        unsigned int m_attribIndex;
        unsigned int m_id;
        unsigned int m_verticeSize;
        unsigned int m_dataSize = 3;
};

#endif // VERTEXBUFFER_H
