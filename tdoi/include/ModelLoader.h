#ifndef MODELLOADER_H
#define MODELLOADER_H
#include <vector>
#include <stdio.h>
#include <string.h>
#include <string>
#include <cstdlib>
// Include GLM
#include <glm/glm.hpp>
#include "glm/gtc/matrix_transform.hpp"

namespace Models{

    struct ModelData{
        std::vector<glm::vec3> vertices;
        std::vector<glm::vec2> uvs;
        std::vector<glm::vec3> normals;
    };

    class ModelLoader
    {
        public:
            ModelLoader();
            ModelData loadOBJ(const char* file_path);

        protected:

        private:
    };

}

#endif // MODELLOADER_H
