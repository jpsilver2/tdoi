#ifndef TEXTUREHANDLER_H
#define TEXTUREHANDLER_H

#include <vector>
#include <string>
#include <Texture.h>

class TextureHandler
{
    public:
        struct TextureInfo{
            Texture* texture;
            const char* name;
        };
        TextureHandler();
        virtual ~TextureHandler();
        Texture* addTexture(const char* imagepath, unsigned char texturebank, const char* name);
        Texture* getTexture(const char* texture_name);
        void cleanTextures();
	std::vector<const char*> getTextureNames();

protected:

    private:
        std::vector<TextureInfo> m_deleted_textures;
        std::vector<TextureInfo> m_texture_batch;
};

#endif // TEXTUREHANDLER_H
