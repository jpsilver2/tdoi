#ifndef SHADERLOADER_H
#define SHADERLOADER_H
#include <Program.h>
#include <fstream>
#include <iostream>
#include <string>

using namespace std;

class ShaderLoader
{
    public:
        ShaderLoader(string shader_path) {
            std::ifstream shader_file;
            shader_file.open(shader_path.c_str());
            string line;
            this->m_shaderSources[0] = "";
            this->m_shaderSources[1] = "";
            unsigned int current_shader = 0;
            while(getline(shader_file, line)){
                if(line.compare("#VERT_SHADER") == 0){
                    current_shader = 0;
                    cout << "Vertex_shader" << endl;
                }else
                if(line.compare("#FRAG_SHADER") == 0 ){
                    current_shader = 1;
                    cout << "Fragment_shader" << endl;
                }else{
                    string source = line + "\n";
                    this->m_shaderSources[current_shader] += source;
                    cout << source << endl;
                }
            }
        }
        virtual ~ShaderLoader() {}
        string getShader(unsigned int index){
            return this->m_shaderSources[index];
        }

    protected:

    private:
        string m_shaderSources[2] = {"", ""};
};

#endif // SHADERLOADER_H
