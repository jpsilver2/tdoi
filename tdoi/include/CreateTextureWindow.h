#pragma once
#include "GuiBase.h"
#include "iamGui\Addons\imguifilesystem\imguifilesystem.h"

class CreateTextureWindow :
	public GuiBase
{
public:
	CreateTextureWindow();
	void Render() override;
	CreateTextureWindow(ResourceHandler* rh, EntityHandler* eh);
private:
	int m_texture_bank;
	char m_texture_name_[32];
	char m_texture_path_[128];
};

