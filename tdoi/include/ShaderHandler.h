#ifndef SHADERHANDLER_H
#define SHADERHANDLER_H

#include <vector>
#include <string>
#include <Program.h>


class ShaderHandler
{
    public:
        struct ShaderInfo{
            Program* shader;
            const char* name;
        };
        ShaderHandler();
        virtual ~ShaderHandler();
        Program* addShader(const char* shaderpath, const char* name);
        Program* getShader(const char* shader_name);
        void cleanShaders();
	vector<const char*> getShaderNames();

protected:

    private:
        std::vector<ShaderInfo> m_deletedShaders;
        std::vector<ShaderInfo> m_shaderBatch;
};

#endif // SHADERHANDLER_H
