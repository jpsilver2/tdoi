#ifndef PROGRAM_H
#define PROGRAM_H

#include <ShaderLoader.h>

#include <string>

#include <Shader.h>

#include <vector>

// Include GLM
#include <glm/glm.hpp>

class Program
{
    public:
        Program(std::string shader_path);
        virtual ~Program();
        void useProgram(glm::mat4 mvp);
        void Delete();
        bool isDeleted();

    protected:
        Shader m_shaders[2] = {Shader(), Shader()};
        unsigned int m_program_id;
        bool m_deleted = false;

    private:

};

#endif // PROGRAM_H
