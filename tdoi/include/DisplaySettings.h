#ifndef DISPLAYSETTINGS_H
#define DISPLAYSETTINGS_H
#include <iostream>
#include <string>
#include <fstream>

class DisplaySettings
{
    public:
        DisplaySettings();
        DisplaySettings(std::string filename);
        DisplaySettings(unsigned int width, unsigned int height, unsigned int samples,
                        unsigned int gl_major, unsigned int gl_minor, bool is_forward_compatable,
                        std::string title);
        unsigned int getWidth();
        unsigned int getHeight();
        unsigned int getSamples();
        unsigned int getGLMajor();
        unsigned int getGLMinor();
        bool isForwardCompatable();
        std::string getTitle();

    protected:

    private:
        unsigned int m_width ,
                            m_height,
                            m_samples,
                            m_gl_major,
                            m_gl_minor;
        bool forward_compatable;

        std::string m_title;

};

#endif // DISPLAYSETTINGS_H
