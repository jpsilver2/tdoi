#pragma once
#include <glm/glm.hpp>
#include "glm/gtc/matrix_transform.hpp"
#include "GuiBase.h"
#include "iamGui/Addons/imguifilesystem/imguifilesystem.h"
#include <vector>

class CreateEntityWindow :
	public GuiBase
{
public:
	void Render() override;
	CreateEntityWindow();
	CreateEntityWindow(ResourceHandler* rh, EntityHandler* eh);
private:
	char m_name_[32];
	char m_material_[32];
	char m_model_[32];
	const char* ERROR_MESSAGE = "Invalid material.";
	float m_position[3] = { 0, 0, 0 };
	float m_rotation[3] = { 0, 0, 0 };
	float m_scale[3] = { 1, 1, 1 };
};