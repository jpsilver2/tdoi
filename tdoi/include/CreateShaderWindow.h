#pragma once
#include "GuiBase.h"
#include "iamGui/Addons/imguifilesystem/imguifilesystem.h"

class CreateShaderWindow :
	public GuiBase
{
public:
	CreateShaderWindow();
	CreateShaderWindow(ResourceHandler* rh, EntityHandler* eh);
	void Render() override;
private:
	char m_shader_name_c_[32];
	char m_shader_path_c_[ImGuiFs::MAX_PATH_BYTES];

};

