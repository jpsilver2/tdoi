#pragma once
#include <string>
#include "iamGui\imgui.h"
#include "ResourceHandler.h"
#include "EntityHandler.h"

class GuiBase
{
public:
	GuiBase();
	GuiBase(ResourceHandler* rs, EntityHandler* eh);
	virtual void Render();
	~GuiBase();
protected:
	bool m_isOpen = true;
	bool m_canMove = false;
	char* m_title;
	unsigned char error_code_ = -1;
	ResourceHandler * m_p_resource_handler_;
	EntityHandler* m_p_entity_handler_;
};

