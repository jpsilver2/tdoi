#ifndef TEXTURE_H
#define TEXTURE_H

// Include GLEW. Always include it before gl.h and glfw3.h, since it's a bit magic.
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>
#include <nv_dds.h>

#include <soil.h>

using namespace nv_dds;

class Texture
{
    public:
        Texture();
        Texture(const char * texture_path, unsigned char texture_bank);
        virtual ~Texture();
        void Delete();
        unsigned int getTextureId();
        bool isDeleted();
        void bind();

    protected:

    private:
        unsigned char m_texture_bank;
        unsigned int m_id;
        bool m_deleted = false;
};

#endif // TEXTURE_H
