#ifndef ENTITY_H
#define ENTITY_H

#include <string>
#include <RawModel.h>
#include <Material.h>
#include <glm/glm.hpp>
#include "glm/gtc/matrix_transform.hpp"
#include "Object.h"

class entity
{
public:
    entity(object* obj);
    void setPosition(glm::vec3 position);
    void setRotation(glm::vec3 rotation);
    void setScale(glm::vec3 scale);

	void on_create();
	void on_update();
	void on_draw();

    virtual ~entity();
    void draw(glm::mat4 view, glm::mat4 proj);
    bool isDeleted();
    void Delete();

protected:

private:
	object * m_p_object_;
    void updateMatrix();

    bool m_deleted;
    glm::mat4 m_modelMatrix;
    glm::vec3 m_position = glm::vec3(0, 0, 0);
    glm::vec3 m_rotation = glm::vec3(0, 0, 0);
    glm::vec3 m_scale = glm::vec3(1, 1, 1);
};

#endif // ENTITY_H
