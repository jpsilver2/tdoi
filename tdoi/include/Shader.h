#ifndef SHADER_H
#define SHADER_H

// Include GLEW. Always include it before gl.h and glfw3.h, since it's a bit magic.
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

#include <GL/gl.h>

#include <string>

#include <vector>

#include <iostream>

class Shader
{
    public:
        Shader();
        Shader(GLenum shader_type, std::string data);
        virtual ~Shader();
        unsigned int getShaderId();
        GLenum getShaderType();
    protected:

    private:
        GLint compileShaderCode(std::string shader_code);
        unsigned int m_id;
        GLenum m_shaderType;
};

#endif // SHADER_H
