#ifndef TEXTURELOADER_H
#define TEXTURELOADER_H

#include <stdio.h>
#include <string.h>
#include <cstdlib>

// Include GLEW. Always include it before gl.h and glfw3.h, since it's a bit magic.
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>


class TextureLoader
{
    public:
        TextureLoader();
        virtual ~TextureLoader();
        GLuint loadBMP_texture(const char* imagepath);


    protected:

    private:
};

#endif // TEXTURELOADER_H
