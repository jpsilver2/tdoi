#ifndef ENTITYHANDLER_H
#define ENTITYHANDLER_H

#include <vector>
#include "Entity.h"


class EntityHandler
{
    public:
        struct EntityInfo{
            entity* entity;
            const char* name;
        };
        EntityHandler();
        virtual ~EntityHandler();
        entity* addEntity(Material* material, RawModel* raw_model, const char* name);
        entity* getEntity(const char* entity_name);
        void cleanEntities();
        void drawEntities(glm::mat4 View, glm::mat4 Projection);

    protected:

    private:
        std::vector<EntityInfo> m_deletedEntities;
        std::vector<EntityInfo> m_entityBatch;
};

#endif // ENTITYHANDLER_H
