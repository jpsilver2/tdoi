#ifndef RESOURCEHANDLER_H
#define RESOURCEHANDLER_H

#include <vector>
#include <TextureHandler.h>
#include <ShaderHandler.h>
#include <MaterialHandler.h>
#include <VertexBuffer.h>
#include <RawModel.h>
#include "Object.h"
#include <windows.h>

typedef shared_object*(__stdcall* CreateObject)();

class ResourceHandler
{
public:  
	struct raw_model_info
	{
		RawModel* model;
		const char* name;
	};
    struct vertex_buffer_info{
         VertexBuffer* vertexBuffer;
         const char* name;
    };
	struct object_info
	{
		object* object;
		const char* name;
	};
	struct  shared_object_info
	{
		shared_object* shared_object;
		const char* name;
	};

    ResourceHandler();
    virtual ~ResourceHandler();
	RawModel* create_raw_model(unsigned int num_buffers, std::initializer_list<VertexBuffer*> args, const char* name);
	RawModel* get_raw_model(const char* name);

    VertexBuffer* createVertexBuffer(float *data, unsigned int data_count, unsigned int attrib, const char* name);
    VertexBuffer* createVertexBuffer(std::vector<glm::vec3> data, unsigned int attrib, const char* name);
    VertexBuffer* createVertexBuffer(std::vector<glm::vec2> data, unsigned int attrib, const char* name);
		

    VertexBuffer* getVertexBuffer(const char* name);

    Texture* loadTexture(const char* image_path, unsigned char texture_bank, const char* texture_name);
    Program* loadProgram(const char* shader_path, const char* shader_name);
    Material* createMaterial(unsigned char surface_type, const char* image_path, unsigned char texture_bank,
                                                const char* shader_path, const char* texture_name, const char* shader_name,
                                                const char* material_name);
    Material* createMaterial(unsigned char surface_type, const char* texture_name, const char* shader_name, const char* material_name);
    Material* createMaterial(unsigned char surface_type, Texture* texture, Program* program, const char* material_name);

    Texture* getTexture(const char* texture_name);
    Program* getShader(const char* shader_name);
    Material* getMaterial(const char* material_name);

	object* create_object(Material* material, RawModel* raw_model, shared_object* shared_object, const char* name);
	object* get_object(const char* name);

	shared_object* add_shared_object(const char* name);
	shared_object* get_shared_object(const char* name);

	vector<const char*> getMaterialNames();
	vector<const char*> getModelNames();
	vector<const char*> getTextureNames();
	vector<const char*> getShaderNames();

private:
	HINSTANCE h_code_base;
	std::vector<vertex_buffer_info> m_vertexBuffers;
	std::vector<raw_model_info> m_model_batch;
	std::vector<object_info> m_object_batch;
	std::vector<shared_object_info> m_shared_object_batch;
    TextureHandler m_textureHandler = TextureHandler();
    ShaderHandler m_shaderHandler = ShaderHandler();
    MaterialHandler m_materialHandler = MaterialHandler();
};

#endif // RESOURCEHANDLER_H
