#pragma once
// Include GLEW. Always include it before gl.h and glfw3.h, since it's a bit magic.
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "iamGui\imgui.h"
#include "iamGui\imgui_impl_glfw_gl3.h"
#include <GuiBase.h>
#include <vector>

class WindowHandler
{
public:
	struct guiWindowInfo {
		GuiBase* window;
		const char* name;
	};
	WindowHandler();
	~WindowHandler();
	GuiBase* addWindow(GuiBase* window, const char* name);
	GuiBase* getWindow(const char* name);
	void RenderWindows();

private:
	std::vector<guiWindowInfo> m_windows;
};

