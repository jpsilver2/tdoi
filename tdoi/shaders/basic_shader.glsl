#VERT_SHADER
#version 330 core
layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec2 texCoords;
out vec2 UV;
// Values that stay constant for the whole mesh.
uniform mat4 MVP;

void main(){
    gl_Position = MVP * vec4(vertexPosition_modelspace, 1);
	UV = texCoords;
}
#FRAG_SHADER
#version 330 core

in vec2 UV;
out vec3 color;

uniform sampler2D texture_diffuse;

void main(){
  color = texture(texture_diffuse, UV).rgb;
}