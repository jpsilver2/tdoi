#include "..\include\CreateShaderWindow.h"

static ImGuiFs::Dialog dlg;

CreateShaderWindow::CreateShaderWindow()
{
}

CreateShaderWindow::CreateShaderWindow(ResourceHandler* rh, EntityHandler* eh) : GuiBase(rh, eh)
{
}

void CreateShaderWindow::Render()
{
	ImGui::Begin("Create Shader", &m_isOpen, ImGuiWindowFlags_MenuBar);
	switch (error_code_) {
	case -1:
		break;
	case -2:
		ImGui::TextColored(ImVec4(0.0f, 1.0f, 0.0f, 1.0f), "Created Shader");
		break;
	case 0:
		ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), "Failed to create shader...");
	}

	const bool import_shader = ImGui::Button("Import Shader");
	ImGui::SameLine();
	const bool export_shader = ImGui::Button("Export Shader");
	ImGui::SameLine();
	const bool create_shader = ImGui::Button("Create Shader");
	ImGui::InputText(": Shader Name", m_shader_name_c_, IM_ARRAYSIZE(m_shader_name_c_));
	const bool open_shader = ImGui::Button("Open GLSL File");
	dlg.chooseFileDialog(open_shader);
	if (strlen(dlg.getChosenPath())>0) {
		strcpy(m_shader_path_c_, dlg.getChosenPath());
	}
	if (create_shader) {
		char* shader_name = new char[32];
		memcpy(shader_name, m_shader_path_c_, 32);
		if (m_p_resource_handler_->loadProgram(m_shader_path_c_, shader_name) != nullptr) {
			error_code_ = -2;
		}
		else {
			error_code_ = 0;
		}
	}
	ImGui::Text(m_shader_path_c_);

	ImGui::End();
}
