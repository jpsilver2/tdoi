#include "../include/EntityHandler.h"

EntityHandler::EntityHandler()
{
    //ctor
}

entity* EntityHandler::addEntity(Material* material, RawModel* raw_model, const char* name)
{
    for(std::vector<EntityInfo>::iterator it = this->m_entityBatch.begin(); it != this->m_entityBatch.end(); ++it) {
        /*if((*it).entity->isDeleted()){
            this->m_entityBatch.erase(it);
            this->m_deletedEntities.push_back(*it);
        }*/
    }

	/*entity* entity = new entity(nullptr);
    this->m_entityBatch.push_back({entity, name});*/

    return nullptr;
}

entity* EntityHandler::getEntity(const char* entity_name){
    for(std::vector<EntityInfo>::iterator it = this->m_entityBatch.begin(); it != this->m_entityBatch.end(); ++it) {
        if((*it).name == entity_name){
            return (*it).entity;
        }
    }
    return nullptr;
}

void EntityHandler::cleanEntities(){
    for(std::vector<EntityInfo>::iterator it = this->m_deletedEntities.begin(); it != this->m_deletedEntities.end(); ++it) {
        delete (*it).entity;
    }
}

void EntityHandler::drawEntities(glm::mat4 View, glm::mat4 Projection){
    for(std::vector<EntityInfo>::iterator it = this->m_entityBatch.begin(); it != this->m_entityBatch.end(); ++it) {
        (*it).entity->draw(View, Projection);
    }
}

EntityHandler::~EntityHandler()
{
    for(std::vector<EntityInfo>::iterator it = this->m_entityBatch.begin(); it != this->m_entityBatch.end(); ++it) {
        this->m_entityBatch.erase(it);
        this->m_deletedEntities.push_back(*it);
    }
    this->cleanEntities();
}
