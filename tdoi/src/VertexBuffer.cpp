#include "VertexBuffer.h"
VertexBuffer::VertexBuffer(){
}

VertexBuffer::VertexBuffer(float *data, unsigned int data_cout, unsigned int attrib){
    this->m_attribIndex = attrib;
    this->m_verticeSize = data_cout;
    glGenBuffers(1, &this->m_id);
    glBindBuffer(GL_ARRAY_BUFFER, this->m_id);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * data_cout, data, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

VertexBuffer::VertexBuffer(std::vector<glm::vec3> data, unsigned int attrib){
    this->m_attribIndex = attrib;
    this->m_verticeSize = data.size() * 3;
    glGenBuffers(1, &this->m_id);
    glBindBuffer(GL_ARRAY_BUFFER, this->m_id);
    glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(glm::vec3), &data[0], GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

VertexBuffer::VertexBuffer(std::vector<glm::vec2> data, unsigned int attrib){
    this->m_attribIndex = attrib;
    this->m_verticeSize = data.size() * 2;
    this->m_dataSize = 2;
    glGenBuffers(1, &this->m_id);
    glBindBuffer(GL_ARRAY_BUFFER, this->m_id);
    glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(glm::vec2), &data[0], GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

unsigned int VertexBuffer::getVerticeCount(){
    return this->m_verticeSize;
}

void VertexBuffer::unbind(){
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}//I KNOW YOUR WATCHING.....

void VertexBuffer::enableAttribArray(){
    glEnableVertexAttribArray(this->m_attribIndex);
}

void VertexBuffer::setDataSize(unsigned int size){
    this->m_dataSize = size;
}

void VertexBuffer::disableAttribArray(){
    glDisableVertexAttribArray(this->m_attribIndex);
}
void VertexBuffer::bind(){
    glBindBuffer(GL_ARRAY_BUFFER, this->m_id);
    glVertexAttribPointer(
           this->m_attribIndex,
           this->m_dataSize,                  // size
           GL_FLOAT,           // type
           GL_FALSE,           // normalized?
           0,                  // stride
           (void*)0            // array buffer offset
        );
}

VertexBuffer::~VertexBuffer()
{
    glDeleteBuffers(1, &this->m_id);
}
