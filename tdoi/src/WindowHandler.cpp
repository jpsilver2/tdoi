#include "..\include\WindowHandler.h"



WindowHandler::WindowHandler()
{
}


WindowHandler::~WindowHandler()
{
	for (std::vector<guiWindowInfo>::iterator it = this->m_windows.begin(); it != this->m_windows.end(); ++it) {
		delete (*it).window;
	}
}

GuiBase* WindowHandler::addWindow(GuiBase* window, const char * name)
{
	m_windows.push_back({ window, name });
	return window;
}

void WindowHandler::RenderWindows()
{
	glDisable(GL_SCISSOR_TEST);
	ImGui_ImplGlfwGL3_NewFrame();

	for (std::vector<guiWindowInfo>::iterator it = this->m_windows.begin(); it != this->m_windows.end(); ++it) {
		(*it).window->Render();
	}

	ImGui::Render();
	ImGui_ImplGlfwGL3_RenderDrawData(ImGui::GetDrawData());
}
