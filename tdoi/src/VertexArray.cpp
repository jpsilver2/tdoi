#include "VertexArray.h"

VertexArray::VertexArray(){

}

VertexArray::VertexArray(VertexBuffer *vertexBuffer)
{
    glGenVertexArrays(1, &this->m_id);
    glBindVertexArray(this->m_id);
    this->m_pVertexBuffer = new VertexBuffer* [1];
    this->m_bufferCount = 1;
    this->m_pVertexBuffer[0] = vertexBuffer;
}

VertexArray::VertexArray(VertexBuffer **vertexBuffer, unsigned int num_buffers){
    glGenVertexArrays(1, &this->m_id);
    glBindVertexArray(this->m_id);
    this->m_pVertexBuffer = new VertexBuffer* [num_buffers];
    this->m_bufferCount = num_buffers;
    for(int i = 0; i < num_buffers; i ++){
        VertexBuffer *tmp = vertexBuffer[i];
        this->m_pVertexBuffer[i] = *vertexBuffer;
    }
}

VertexArray::VertexArray(unsigned int size, std::initializer_list<VertexBuffer*> args){
    glGenVertexArrays(1, &this->m_id);
    glBindVertexArray(this->m_id);
    this->m_bufferCount = size;
    this->m_pVertexBuffer = new VertexBuffer*[size];
    auto i = 0;
    for (auto arg: args) {
        this->m_pVertexBuffer[i ++] = arg;
    }
}

void VertexArray::draw(){
    bind();
    //for(int i = 0; i < this->m_bufferCount; i ++){
        this->m_pVertexBuffer[1]->enableAttribArray();
        this->m_pVertexBuffer[0]->enableAttribArray();
        this->m_pVertexBuffer[1]->bind();
    //}
    glDrawArrays(GL_TRIANGLES, 0, this->m_pVertexBuffer[0]->getVerticeCount()/3); // Starting from vertex 0; 3 vertices total -> 1 triangle

    this->m_pVertexBuffer[0]->bind();
    //(int i = 0; i < this->m_bufferCount; i ++){
        //this->m_pVertexBuffer[i]->unbind();
    //}
}

void VertexArray::bind(){
    glBindVertexArray(this->m_id);
    //this->m_vertexBuffer.bind();
}

VertexArray::~VertexArray()
{
    //dtor
}
