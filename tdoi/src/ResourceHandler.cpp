#include "../include/ResourceHandler.h"

ResourceHandler::ResourceHandler()
{
    //ctor
}

VertexBuffer* ResourceHandler::getVertexBuffer(const char* name){
    for(auto it = this->m_vertexBuffers.begin(); it != this->m_vertexBuffers.end(); ++it) {
        if((*it).name == name){
            return (*it).vertexBuffer;
        }
    }
    return nullptr;
}

VertexBuffer* ResourceHandler::createVertexBuffer(float *data, unsigned int data_count, unsigned int attrib, const char* name){
	auto* buffer = new VertexBuffer(data, data_count, attrib);
    m_vertexBuffers.push_back({buffer, name});
    return buffer;
}

VertexBuffer* ResourceHandler::createVertexBuffer(std::vector<glm::vec3> data, unsigned int attrib, const char* name){
    VertexBuffer* buffer = new VertexBuffer(data, attrib);
    m_vertexBuffers.push_back({buffer, name});
    return buffer;
}

VertexBuffer* ResourceHandler::createVertexBuffer(std::vector<glm::vec2> data, unsigned int attrib, const char* name){
    VertexBuffer* buffer = new VertexBuffer(data, attrib);
    m_vertexBuffers.push_back({buffer, name});
    return buffer;
}

Texture* ResourceHandler::loadTexture(const char* image_path, unsigned char texture_bank, const char* texture_name){
    return this->m_textureHandler.addTexture(image_path, texture_bank, texture_name);
}

Program* ResourceHandler::loadProgram(const char* shader_path, const char* shader_name){
    return this->m_shaderHandler.addShader(shader_path, shader_name);
}

Material* ResourceHandler::createMaterial(unsigned char surface_type, const char* image_path, unsigned char texture_bank,
                                                const char* shader_path, const char* texture_name, const char* shader_name,
                                                const char* material_name){
    Texture* texture = m_textureHandler.addTexture(image_path, texture_bank, texture_name);
    Program* program = m_shaderHandler.addShader(shader_path, shader_name);
    return m_materialHandler.addMaterial(surface_type, program, texture, material_name);
}

Material* ResourceHandler::createMaterial(unsigned char surface_type, const char* texture_name, const char* shader_name, const char* material_name){
    return m_materialHandler.addMaterial(surface_type, m_shaderHandler.getShader(shader_name), m_textureHandler.getTexture(texture_name), material_name);
}

Material* ResourceHandler::createMaterial(unsigned char surface_type, Texture* texture, Program* program, const char* material_name){
    return m_materialHandler.addMaterial(surface_type, program, texture, material_name);
}

Texture* ResourceHandler::getTexture(const char* texture_name){
    return this->m_textureHandler.getTexture(texture_name);
}

Program* ResourceHandler::getShader(const char* shader_name){
    return this->m_shaderHandler.getShader(shader_name);
}

Material* ResourceHandler::getMaterial(const char* material_name){
    return this->m_materialHandler.getMaterial(material_name);
}

object* ResourceHandler::create_object(Material* material, RawModel* raw_model, shared_object* shared_object,
	const char* name)
{
	object* obj = new object(material, raw_model, shared_object);
	m_object_batch.push_back({ obj, name });
	return obj;
}

object* ResourceHandler::get_object(const char* name)
{
	for (auto it = this->m_object_batch.begin(); it != this->m_object_batch.end(); ++it) {
		auto current_name = std::string((*it).name);
		auto search_name = std::string(name);
		if (current_name.compare(search_name) == 0) {
			return (*it).object;
		}
	}
	return nullptr;
}

shared_object* ResourceHandler::add_shared_object(const char* name)
{
	CreateObject create_shared = (CreateObject)GetProcAddress(h_code_base, name)();
	shared_object* shared = create_shared();
	m_shared_object_batch.push_back({ shared, name });
	return shared;
}

shared_object* ResourceHandler::get_shared_object(const char* name)
{
	for (auto it = this->m_shared_object_batch.begin(); it != this->m_shared_object_batch.end(); ++it) {
		auto current_name = std::string((*it).name);
		auto search_name = std::string(name);
		if (current_name.compare(search_name) == 0) {
			return (*it).shared_object;
		}
	}
	return nullptr;
}

vector<const char*> ResourceHandler::getMaterialNames()
{
	return this->m_materialHandler.getMaterialNames();
}

vector<const char*> ResourceHandler::getModelNames()
{
	vector<const char*> model_names;
	for (auto it = this->m_model_batch.begin(); it != this->m_model_batch.end(); ++it) {
		model_names.push_back((*it).name);
	}
	return model_names;
}

vector<const char*> ResourceHandler::getTextureNames()
{
	return m_textureHandler.getTextureNames();
}

vector<const char*> ResourceHandler::getShaderNames()
{
	return m_shaderHandler.getShaderNames();
}

ResourceHandler::~ResourceHandler()
{
    //dtor
}

RawModel * ResourceHandler::create_raw_model(unsigned int num_buffers, std::initializer_list<VertexBuffer*> args, const char * name)
{
	auto* model = new RawModel(num_buffers, args);
	m_model_batch.push_back({ model, name });
	return model;
}

RawModel* ResourceHandler::get_raw_model(const char* name)
{
	for (auto it = this->m_model_batch.begin(); it != this->m_model_batch.end(); ++it) {
		auto current_name = std::string((*it).name);
		auto search_name = std::string(name);
		if (current_name.compare(search_name) == 0) {
			return (*it).model;
		}
	}
	return nullptr;
}
