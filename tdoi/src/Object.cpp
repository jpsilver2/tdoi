#include "..\include\Object.h"


object::object(Material* material, RawModel* raw_model, shared_object* shared_object)
{
	this->m_pMaterial = material;
	this->m_p_raw_model = raw_model;
}

object::~object()
{
}

void object::init_pointers(vec3f position, vec3f rotation, vec3f scale)
{
	this->m_p_shared_object_->init_pointers(position, rotation, scale);
}

void object::on_create()
{
	this->m_p_shared_object_->on_create();
}

void object::on_update()
{
	this->on_update();
}

void object::on_draw()
{
	this->on_draw();
}

void object::useMaterial(glm::mat4 mvp)
{
	this->m_pMaterial->useMaterial(mvp);
}

void object::draw()
{
	this->m_p_raw_model->draw();
}
