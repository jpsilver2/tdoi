#include "../include/Display.h"

ResourceHandler RH = ResourceHandler();
EntityHandler EH = EntityHandler();
WindowHandler WH = WindowHandler();

double xpos, ypos;

bool track_mouse = false;
double deltaTime = 0;

Camera camera = Camera();

ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

Display::Display(DisplaySettings settings){
    this->m_settings = settings;
	this->initializeWindow();
    glfwWindowHint(GLFW_SAMPLES, settings.getSamples()); // 4x antialiasing
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, settings.getGLMajor()); // We want OpenGL 3.3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, settings.getGLMinor());
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, settings.isForwardCompatable()); // To make MacOS happy; should not be needed
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); // We don't want the old OpenGL
    this->createWindow();
}

bool Display::hasError(){
    return this->m_errorCode != -1;
}

void Display::enableGLConstants(){
    // Enable depth test
    glEnable(GL_DEPTH_TEST);
    // Accept fragment if it closer to the camera than the former one
    glDepthFunc(GL_LESS);
    // Cull triangles which normal is not towards the camera
    glEnable(GL_CULL_FACE);
}

void Display::load(){
	RH.createMaterial(0, "./data/textures/grid.dds", 0, ".shaders/grid_shader.glsl", "t_grid", "s_grid", "m_grid");
	//RH.createVertexBuffer(data, )
}

void Display::init(){
	ImGui::CreateContext();
	ImGui_ImplGlfwGL3_Init(this->m_pWindow, true);
	ImGui::StyleColorsDark();
	WH.addWindow(new CreateEntityWindow(&RH, &EH), "w_create_entity");
	WH.addWindow(new CreateMaterialWindow(&RH, &EH), "w_create_material");
	WH.addWindow(new CreateTextureWindow(&RH, &EH), "w_create_texture");
	WH.addWindow(new CreateShaderWindow(&RH, &EH), "w_create_shader");

	glfwSetCursorPos(this->m_pWindow, this->m_settings.getWidth() / 2, this->m_settings.getHeight() / 2);
	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(this->m_pWindow, GLFW_STICKY_KEYS, GL_TRUE);

	enableGLConstants();
}

void Display::handleMouse()
{
	glfwGetCursorPos(this->m_pWindow, &xpos, &ypos);
	if (!track_mouse) {
		xpos = m_settings.getWidth() / 2;
		ypos = m_settings.getHeight() / 2;
	}
	else {
		glfwSetCursorPos(this->m_pWindow, this->m_settings.getWidth() / 2, this->m_settings.getHeight() / 2);
	}
}

void Display::handleInput()
{
	// Move forward
	if (glfwGetKey(this->m_pWindow, GLFW_KEY_W) == GLFW_PRESS) {
		camera.moveForward(deltaTime);
	}
	// Move backward
	if (glfwGetKey(this->m_pWindow, GLFW_KEY_S) == GLFW_PRESS) {
		camera.moveBackwards(deltaTime);
	}
	// Strafe right
	if (glfwGetKey(this->m_pWindow, GLFW_KEY_D) == GLFW_PRESS) {
		camera.moveRight(deltaTime);
	}
	// Strafe left
	if (glfwGetKey(this->m_pWindow, GLFW_KEY_A) == GLFW_PRESS) {
		camera.moveLeft(deltaTime);
	}

	if (glfwGetKey(this->m_pWindow, GLFW_KEY_T) == GLFW_PRESS) {
		track_mouse = true;
	}

	if (glfwGetKey(this->m_pWindow, GLFW_KEY_Y) == GLFW_PRESS) {
		track_mouse = false;
	}

	if (glfwGetKey(this->m_pWindow, GLFW_KEY_E) == GLFW_PRESS) {
		camera.translate(glm::vec3(0, camera.getSpeed() * deltaTime, 0));
	}

	if (glfwGetKey(this->m_pWindow, GLFW_KEY_Q) == GLFW_PRESS) {
		camera.translate(glm::vec3(0, -camera.getSpeed() * deltaTime, 0));
	}
	if (glfwGetKey(this->m_pWindow, GLFW_KEY_ESCAPE) == GLFW_PRESS || glfwWindowShouldClose(this->m_pWindow) != 0) {
		isRunning = false;
	}
}

void Display::input()
{
	handleMouse();
	handleInput();
}

void Display::update()
{
	camera.update(deltaTime, xpos, ypos);
}

void Display::draw()
{
	EH.drawEntities(camera.getCameraMatrix(), getProjectionMatrix());
	WH.RenderWindows();
}

double Display::beginRender()
{
	glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	return glfwGetTime();
}

void Display::endRender(double startTime)
{
	deltaTime = glfwGetTime() - startTime;
	glfwSwapBuffers(this->m_pWindow);
	glfwPollEvents();
}

void Display::gameLoop(){
	double startTime;
	load();
    init();
    do{
		startTime = beginRender();
		input();
		update();
		draw();
		endRender(startTime);
    } // Check if the ESC key was pressed or the window was closed
    while(isRunning);

	ImGui_ImplGlfwGL3_Shutdown();
	ImGui::DestroyContext();
	glfwTerminate();
}

void Display::initializeWindow(){
    // Initialise GLFW
    if( !glfwInit() )
    {
        fprintf( stderr, "Failed to initialize GLFW\n" );
        m_errorCode = 1;
    }


}

void Display::createWindow(){
    this->m_pWindow = glfwCreateWindow( m_settings.getWidth(), m_settings.getHeight(), m_settings.getTitle().c_str(), NULL, NULL);
    if( m_pWindow == NULL ){
        fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
        glfwTerminate();
        this->m_errorCode = 2;
    }

    glfwMakeContextCurrent(m_pWindow); // Initialize GLEW
    glewExperimental=true; // Needed in core profile
    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW\n");
        this->m_errorCode = 3;
    }
}

glm::mat4 Display::getProjectionMatrix(){
    float FOV = 45.0F;
    float N = 0.1F;
    float F = 100.0F;
    return glm::perspective(glm::radians(FOV), (float) this->m_settings.getWidth() / (float)this->m_settings.getHeight(), N, F);
}

Display::~Display()
{
    //delete vertexBuffer;
    //delete uvBuffer;
}

