#include "TextureHandler.h"

TextureHandler::TextureHandler()
{
    //ctor
}

Texture* TextureHandler::addTexture(const char* imagepath, unsigned char texturebank, const char* name)
{
    for(auto it = this->m_texture_batch.begin(); it != this->m_texture_batch.end(); ++it) {
        if((*it).texture->isDeleted()){
            this->m_texture_batch.erase(it);
            this->m_deleted_textures.push_back(*it);
        }
    }

	auto* texture = new Texture(imagepath, texturebank);
    this->m_texture_batch.push_back({texture, name});

    return texture;
}

Texture* TextureHandler::getTexture(const char* texture_name){
    for(auto it = this->m_texture_batch.begin(); it != this->m_texture_batch.end(); ++it) {
		auto current_name = std::string((*it).name);
		auto search_name = std::string(texture_name);
		if (current_name.compare(search_name) == 0) {
			return (*it).texture;
		}
	}
    return nullptr;
}

void TextureHandler::cleanTextures(){
    for(auto it = this->m_deleted_textures.begin(); it != this->m_deleted_textures.end(); ++it) {
		Texture* texture = (*it).texture;
		if (texture != nullptr) {
			delete texture;
		}
    }
}

std::vector<const char*> TextureHandler::getTextureNames()
{
	std::vector<const char*> texture_names;
	for (auto it = this->m_texture_batch.begin(); it != this->m_texture_batch.end(); ++it) {
		texture_names.push_back((*it).name);
	}
	return texture_names;
}

TextureHandler::~TextureHandler()
{
    for(auto it = this->m_texture_batch.begin(); it != this->m_texture_batch.end(); ++it) {
		Texture* texture = (*it).texture;
		if (texture != nullptr) {
			delete texture;
		}
    }
    this->cleanTextures();
}
