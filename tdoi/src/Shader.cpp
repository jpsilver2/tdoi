#include "../include/Shader.h"

Shader::Shader(GLenum shader_type, std::string shader_source){
    this->m_id = glCreateShader(this->m_shaderType = shader_type);
    compileShaderCode(shader_source);
}

Shader::~Shader(){
}

Shader::Shader(){
}

GLint Shader::compileShaderCode(std::string shader_code){
    GLint result = GL_FALSE;
    int infoLogLength;
    // Compile Shader
	printf("Compiling %s shader\n", (this->getShaderType() == GL_VERTEX_SHADER) ? "vertex" : "fragment");
	char const * SourcePointer = shader_code.c_str();
	glShaderSource(this->m_id, 1, &SourcePointer , NULL);
	glCompileShader(this->m_id);

	// Check Shader
	glGetShaderiv(this->m_id, GL_COMPILE_STATUS, &result);
	glGetShaderiv(this->m_id, GL_INFO_LOG_LENGTH, &infoLogLength);
	if ( infoLogLength > 0 ){
		std::vector<char> ShaderErrorMessage(infoLogLength+1);
		glGetShaderInfoLog(this->m_id, infoLogLength, NULL, &ShaderErrorMessage[0]);
		printf("%s\n", &ShaderErrorMessage[0]);
	}
	return result;
}

unsigned int Shader::getShaderId(){
    return this->m_id;
}

GLenum Shader::getShaderType(){
    return this->m_shaderType;
}

