#include "../include/Entity.h"

entity::entity(object* obj)
{
    
	this->m_p_object_ = obj;
	this->m_p_object_->init_pointers({ &m_position.x, &m_position.y, &m_position.z },
											{&m_rotation.x, &m_rotation.y, &m_rotation.z},
											{ &m_scale.x, &m_scale.y, &m_scale.z });
}

void entity::setPosition(glm::vec3 position){
    m_position = position;
    updateMatrix();
}

void entity::updateMatrix(){
    m_modelMatrix = glm::mat4(1);
    m_modelMatrix = glm::translate(m_modelMatrix, this->m_position);
    m_modelMatrix = glm::rotate(m_modelMatrix, m_rotation.x, glm::vec3(1, 0, 0));
    m_modelMatrix = glm::rotate(m_modelMatrix, m_rotation.y, glm::vec3(0, 1, 0));
    m_modelMatrix = glm::rotate(m_modelMatrix, m_rotation.z, glm::vec3(0, 0, 1));
    m_modelMatrix = glm::scale(m_modelMatrix, m_scale);
}

void entity::setRotation(glm::vec3 rotation){
    m_rotation = rotation;
    updateMatrix();
}

void entity::setScale(glm::vec3 scale){
    m_scale = scale;
    updateMatrix();
}

void entity::on_create()
{
	m_p_object_->on_create();
}

void entity::on_update()
{
	m_p_object_->on_update();
}

void entity::on_draw()
{
	m_p_object_->on_draw();
}

void entity::draw(glm::mat4 view, glm::mat4 proj){
    updateMatrix();
    this->m_p_object_->useMaterial(proj * view * m_modelMatrix);
    this->m_p_object_->draw();
	this->on_draw();
}

void entity::Delete(){
    this->m_deleted = true;
}

bool entity::isDeleted(){
    return this->m_deleted;
}

entity::~entity()
{
    //dtor
}
