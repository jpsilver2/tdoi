#include "..\include\CreateMaterialWindow.h"

static ImGuiFs::Dialog dlg;

CreateMaterialWindow::CreateMaterialWindow()
{
}

CreateMaterialWindow::CreateMaterialWindow(ResourceHandler * rh, EntityHandler * eh) : GuiBase(rh, eh)
{
}

void CreateMaterialWindow::Render()
{
	ImGui::Begin("Create Material", &m_isOpen, ImGuiWindowFlags_MenuBar);
	const bool importMaterial = ImGui::Button("Import Material");
	ImGui::SameLine();
	const bool exportMaterial = ImGui::Button("Export Material");
	ImGui::SameLine();
	const bool createMaterial = ImGui::Button("Create Material");
	ImGui::InputText(": Material Name", m_material_name_, 32);
	ImGui::InputInt("Surface Type", &m_surface);
	auto textures_c = m_p_resource_handler_->getTextureNames();
	auto programs_c = m_p_resource_handler_->getShaderNames();

	static const char* selected_texture = NULL;
	static const char* selected_shader = NULL;

	if (ImGui::BeginCombo("##combo_texture", selected_texture)) // The second parameter is the label previewed before opening the combo.
	{
		unsigned short n = 0;
		for (auto it = textures_c.begin(); it != textures_c.end(); ++it) {
			bool is_selected = (selected_texture == textures_c[n]); // You can store your selection however you want, outside or inside your objects
			if (ImGui::Selectable(textures_c[n], is_selected))
				selected_texture = textures_c[n];
			if (is_selected)
				ImGui::SetItemDefaultFocus();
			n++;
		}
		ImGui::EndCombo();
	}
	ImGui::SameLine();
	ImGui::Text(": Texture");

	if (ImGui::BeginCombo("##combo_shader", selected_shader)) // The second parameter is the label previewed before opening the combo.
	{
		unsigned short n = 0;
		for (auto it = programs_c.begin(); it != programs_c.end(); ++it) {
			bool is_selected = (selected_shader == programs_c[n]); // You can store your selection however you want, outside or inside your objects
			if (ImGui::Selectable(programs_c[n], is_selected))
				selected_shader = programs_c[n];
			if (is_selected)
				ImGui::SetItemDefaultFocus();
			n++;
		}
		ImGui::EndCombo();
	}
	ImGui::SameLine();
	ImGui::Text(": Shader");

	if (m_surface < 0) {
		m_surface = 255;
	}
	else if (m_surface > 255) {
		m_surface = 0;
	}
	switch (error_code_) {
	case -1:
		break;
	case 0:
		ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), "Invalid texture name...");
		break;
	case 1:
		ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), "Invalid shader name...");
		break;
	}
	if (createMaterial) {
		Texture* texture = m_p_resource_handler_->getTexture(selected_texture);
		Program* program = m_p_resource_handler_->getShader(selected_shader);
		if (texture == nullptr) {
			error_code_ = 0;
		}
		else if (program == nullptr) {
			error_code_ = 1;
		}
		else {
			error_code_ = -1;
			char *material_name = new char[32];
			memcpy(material_name, m_material_name_, 32);
			m_p_resource_handler_->createMaterial(m_surface, texture, program, material_name);
			memset(m_material_name_, 0, sizeof(m_material_name_));
		}
	}
	ImGui::End();
}