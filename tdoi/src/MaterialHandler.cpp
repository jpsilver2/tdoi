#include "../include/MaterialHandler.h"

MaterialHandler::MaterialHandler()
{
    //ctor
}

Material* MaterialHandler::addMaterial(unsigned char surface_type, Program* shader, Texture* texture, const char* material_name)
{
    for(auto it = this->m_materialBatch.begin(); it != this->m_materialBatch.end(); ++it) {
        if((*it).material->isDeleted()){
            //this->m_materialBatch.erase(it);
            //this->m_deletedMaterials.push_back(*it);
        }
    }

	auto* material = new Material(surface_type, shader, texture);
    this->m_materialBatch.push_back({material, material_name});

    return material;
}

void MaterialHandler::useMaterial(const char* material_name, glm::mat4 mvp){
    this->getMaterial(material_name)->useMaterial(mvp);
}

Material* MaterialHandler::getMaterial(const char* material_name){
    for(auto it = this->m_materialBatch.begin(); it != this->m_materialBatch.end(); ++it) {
		auto current_name = std::string((*it).name);
	    auto search_name = std::string(material_name);
        if(current_name.compare(search_name) == 0){
            return (*it).material;
        }
    }
    return nullptr;
}

void MaterialHandler::cleanMaterials(){
    for(auto it = this->m_deletedMaterials.begin(); it != this->m_deletedMaterials.end(); ++it) {
		auto* mat = (*it).material;
		if (mat != nullptr) {
			delete mat;
		}
    }
}

unsigned int MaterialHandler::getNumberOfMaterials()
{
	return m_materialBatch.size();
}

vector<const char*> MaterialHandler::getMaterialNames()
{
	vector<const char*> names;
	for (auto it = this->m_materialBatch.begin(); it != this->m_materialBatch.end(); ++it) {
		names.push_back((*it).name);
	}
	return names;
}

MaterialHandler::~MaterialHandler()
{
	for (auto it = this->m_materialBatch.begin(); it != this->m_materialBatch.end(); ++it) {
		auto* mat = (*it).material;
		if (mat != nullptr) {
			delete mat;
		}
	}
	cleanMaterials();
}
