#include "../include/Program.h"

Program::Program(std::string shader_path)
{
    ShaderLoader loader(shader_path);
    this->m_shaders[0] = Shader(GL_VERTEX_SHADER, loader.getShader(0));
    this->m_shaders[1] = Shader(GL_FRAGMENT_SHADER, loader.getShader(1));

    GLint result = GL_FALSE;
    int infoLogLength;

	// Link the program
	printf("Linking program\n");
	this->m_program_id = glCreateProgram();
	glAttachShader(this->m_program_id, this->m_shaders[0].getShaderId());
	glAttachShader(this->m_program_id, this->m_shaders[1].getShaderId());
	glLinkProgram(this->m_program_id);

	// Check the program
	glGetProgramiv(this->m_program_id, GL_LINK_STATUS, &result);
	glGetProgramiv(this->m_program_id, GL_INFO_LOG_LENGTH, &infoLogLength);
	if ( infoLogLength > 0 ){
		std::vector<char> ProgramErrorMessage(infoLogLength+1);
		glGetProgramInfoLog(this->m_program_id, infoLogLength, NULL, &ProgramErrorMessage[0]);
		printf("%s\n", &ProgramErrorMessage[0]);
	}


	glDetachShader(this->m_program_id, this->m_shaders[0].getShaderId());
	glDetachShader(this->m_program_id, this->m_shaders[1].getShaderId());

	glDeleteShader(this->m_shaders[0].getShaderId());
	glDeleteShader(this->m_shaders[1].getShaderId());
}

void Program::useProgram(glm::mat4 mvp){
    // Get a handle for our "MVP" uniform
    // Only during the initialisation
    GLuint MatrixID = glGetUniformLocation(this->m_program_id, "MVP");

    // Send our transformation to the currently bound shader, in the "MVP" uniform
    // This is done in the main loop since each model will have a different MVP matrix (At least for the M part)
    glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &mvp[0][0]);

    glUseProgram(this->m_program_id);
}

void Program::Delete(){
    this->m_deleted = true;
}

bool Program::isDeleted(){
    return this->m_deleted;
}

Program::~Program()
{
    //dtor
}
