#include "../include/Camera.h"

Camera::Camera()
{
    //ctor
}
/*
void Camera::translate(float x, float y, float z){
    this->m_position.x += x;
    this->m_position.y += y;
    this->m_position.z += z;
}

void Camera::rotate(float x, float y, float z){
    this->m_rotation.x = x;
    this->m_rotation.y = y;
    this->m_rotation.z = z;
}

void Camera::addRotation(float x, float y, float z){
    this->m_rotation += x;
    this->m_rotation += y;
    this->m_rotation += z;
}

float Camera::getRotationX(){
    return this->m_rotation.x;
}

float Camera::getRotationY(){
    return this->m_rotation.y;
}

float Camera::getRotationZ(){
    return this->m_rotation.z;
}

glm::mat4 Camera::getCameraMatrix(){
    return glm::lookAt(
    glm::vec3(4,3,3), // Camera is at (4,3,3), in World Space
    glm::vec3(0,0,0), // and looks at the origin
    glm::vec3(0,1,0)  // Head is up (set to 0,-1,0 to look upside-down)
    );
}

void Camera::move(float direction, float speed){
    this->m_position.x += speed * cos(direction);
    this->m_position.y += speed * sin(direction);
}*/

glm::mat4 Camera::getCameraMatrix(){
    return glm::lookAt(m_position, m_position + direction, up);
}

void Camera::moveForward(double deltaTime){
    m_position.x += direction.x * deltaTime * speed;
    m_position.y += direction.y * deltaTime * speed;
    m_position.z += direction.z * deltaTime * speed;
}

void Camera::moveBackwards(double deltaTime){
    m_position.x -= direction.x * deltaTime * speed;
    m_position.y -= direction.y * deltaTime * speed;
    m_position.z -= direction.z * deltaTime * speed;
}

void Camera::moveLeft(double deltaTime){
    m_position.x -= right.x * deltaTime * speed;
    m_position.y -= right.y * deltaTime * speed;
    m_position.z -= right.z * deltaTime * speed;
}

void Camera::moveRight(double deltaTime){
    m_position.x += right.x * deltaTime * speed;
    m_position.y += right.y * deltaTime * speed;
    m_position.z += right.z * deltaTime * speed;
}

void Camera::translate(glm::vec3 translation)
{
	m_position += translation;
}

float Camera::getSpeed()
{
	return speed;
}

void Camera::update(double deltaTime, double mx, double my){
    int width = 1024;
    int height = 768;
    // Compute new orientation
    horizontalAngle += mouseSpeed * deltaTime * float(width/2 - mx );
    verticalAngle   += mouseSpeed * deltaTime * float( height/2 - my );
    float x = cos(verticalAngle) * sin(horizontalAngle);
    float y = sin(verticalAngle);
    float z = cos(verticalAngle) * cos(horizontalAngle);

    // Direction : Spherical coordinates to Cartesian coordinates conversion
    direction = glm::vec3(x, y, z);

    right = glm::vec3(
        sin(horizontalAngle - 3.14f/2.0f),
        0,
        cos(horizontalAngle - 3.14f/2.0f)
    );

    // Up vector : perpendicular to both direction and right
    up = glm::cross( right, direction );

}

Camera::~Camera()
{
    //dtor
}
