#include "..\include\CreateTextureWindow.h"

static ImGuiFs::Dialog dlg;

CreateTextureWindow::CreateTextureWindow()
{
}

void CreateTextureWindow::Render()
{
	ImGui::Begin("Create Texture", &m_isOpen, ImGuiWindowFlags_MenuBar);
	switch (error_code_) {
	case -1:
		break;
	case -2:
		ImGui::TextColored(ImVec4(0.0f, 1.0f, 0.0f, 1.0f), "Created Texture");
		break;
	case 0:
		ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), "Failed to create texture...");
	}
	const bool inport_texture = ImGui::Button("Import Texture");
	ImGui::SameLine();
	const bool export_texture = ImGui::Button("Export Texture");
	ImGui::SameLine();
	const bool create_texture = ImGui::Button("Create Texture");
	ImGui::InputText(": Texture Name", m_texture_name_, IM_ARRAYSIZE(m_texture_name_));
	ImGui::InputInt(": Texture Bank", &m_texture_bank);
	const bool open_texture = ImGui::Button("Open Texture Image");
	const char* texture_path = dlg.chooseFileDialog(open_texture);
	if (strlen(texture_path) > 0) {
		for (int i = 0; i < strlen(texture_path); i ++) {
			m_texture_path_[i] = texture_path[i];
		}
	}
	if (create_texture) {
		char *texture_name = new char[32];
		memcpy(texture_name, m_texture_name_, 32);
		if (m_p_resource_handler_->loadTexture(m_texture_path_, m_texture_bank, texture_name) != nullptr) {
			error_code_ = -2;
		}else {
			error_code_ = 0;
		}
	}
	ImGui::Text(m_texture_path_);
	ImGui::End();
}

CreateTextureWindow::CreateTextureWindow(ResourceHandler * rh, EntityHandler * eh) : GuiBase(rh, eh)
{
}
