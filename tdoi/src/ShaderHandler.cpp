#include "../include/ShaderHandler.h"

ShaderHandler::ShaderHandler()
{
    //ctor
}

Program* ShaderHandler::addShader(const char* shaderpath, const char* name)
{
    for(auto it = this->m_shaderBatch.begin(); it != this->m_shaderBatch.end(); ++it) {
        if((*it).shader->isDeleted()){
            this->m_shaderBatch.erase(it);
            this->m_deletedShaders.push_back(*it);
        }
    }

    Program* program = new Program(shaderpath);
    this->m_shaderBatch.push_back({program, name});

    return program;
}

Program* ShaderHandler::getShader(const char* shader_name){
    for(auto it = this->m_shaderBatch.begin(); it != this->m_shaderBatch.end(); ++it) {
        if((*it).name == shader_name){
            return (*it).shader;
        }
    }
    return nullptr;
}

void ShaderHandler::cleanShaders(){
    for(auto it = this->m_deletedShaders.begin(); it != this->m_deletedShaders.end(); ++it) {
		Program* program = (*it).shader;
		if (program != nullptr) {
			delete program;
		}
    }
}

vector<const char*> ShaderHandler::getShaderNames()
{
	std::vector<const char*> shader_names;
	for (auto it = this->m_shaderBatch.begin(); it != this->m_shaderBatch.end(); ++it) {
		shader_names.push_back((*it).name);
	}
	return shader_names;
}


ShaderHandler::~ShaderHandler()
{
    for(auto it = this->m_shaderBatch.begin(); it != this->m_shaderBatch.end(); ++it) {
		Program* program = (*it).shader;
		if (program != nullptr) {
			delete program;
		}
    }
    this->cleanShaders();
}
