#include "Texture.h"

Texture::Texture()
{
    //ctor
}

Texture::Texture(const char * texture_path, unsigned char texture_bank){
	this->m_texture_bank = texture_bank;

    this->m_id = SOIL_load_OGL_texture // load an image file directly as a new OpenGL texture 
	(
		texture_path,
		SOIL_LOAD_AUTO,
		SOIL_CREATE_NEW_ID,
		SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT
	);
}

unsigned int Texture::getTextureId(){
    return this->m_id;
}

void Texture::bind(){
    // "Bind" the newly created texture : all future texture functions will modify this texture
    glBindTexture(GL_TEXTURE_2D, this->m_id);
}

void Texture::Delete(){
    this->m_deleted = true;
}

bool Texture::isDeleted(){
    return this->m_deleted;
}

Texture::~Texture()
{
    //dtor
}
