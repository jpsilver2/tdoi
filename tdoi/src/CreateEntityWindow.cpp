#include "CreateEntityWindow.h"

void CreateEntityWindow::Render()
{
	ImGui::Begin("Create Entity", &m_isOpen, ImGuiWindowFlags_MenuBar);
	if (error_code_ != -1) {
		switch (error_code_) {
		case 0:
			ImGui::Text("INVALID MATERIAL!");
			break;
		case 1:
			ImGui::Text("INVALID MODEL!");
			break;
		default:
			ImGui::Text("UNKNOWN ERROR!");
		}
		const bool import_entity = ImGui::Button("Import");
		ImGui::SameLine();
		const bool export_entity = ImGui::Button("Export");
		ImGui::SameLine();
		const bool test_entity = ImGui::Button("Create");

		auto materials_c = m_p_resource_handler_->getMaterialNames();
		auto models_c = m_p_resource_handler_->getModelNames();

		static const char* selected_material = NULL;
		static const char* selected_model = NULL;

		if (ImGui::BeginCombo("##combo_material", selected_material)) // The second parameter is the label previewed before opening the combo.
		{
			for (auto it = materials_c.begin(); it != materials_c.end(); ++it) {
				bool is_selected = (selected_material == (*it)); // You can store your selection however you want, outside or inside your objects
				if (ImGui::Selectable((*it), is_selected))
					selected_material = (*it);
				if (is_selected)
					ImGui::SetItemDefaultFocus();
			}
			ImGui::EndCombo();
		}
		ImGui::SameLine();
		ImGui::Text(": Material");

		if (ImGui::BeginCombo("##combo_model", selected_model)) // The second parameter is the label previewed before opening the combo.
		{
			for (auto it = models_c.begin(); it != models_c.end(); ++it) {
				bool is_selected = (selected_model == (*it)); // You can store your selection however you want, outside or inside your objects
				if (ImGui::Selectable((*it), is_selected))
					selected_model = (*it);
				if (is_selected)
					ImGui::SetItemDefaultFocus();
			}
			ImGui::EndCombo();
		}
		ImGui::SameLine();
		ImGui::Text(": Model");
		if (test_entity)
		{
			auto* mat = m_p_resource_handler_->getMaterial(selected_material);
			auto* model = m_p_resource_handler_->get_raw_model(selected_model);
			if (mat == nullptr)
			{
				error_code_ = 0;
			}
			else if (model == nullptr)
			{
				error_code_ = 1;
			}
			else
			{
				error_code_ = -1;
			}
		}
	}
		ImGui::End();
}

	CreateEntityWindow::CreateEntityWindow()
{
}

CreateEntityWindow::CreateEntityWindow(ResourceHandler * rh, EntityHandler * eh) : GuiBase(rh, eh)
{
	
}
