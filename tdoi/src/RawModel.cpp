#include "../include/RawModel.h"

RawModel::RawModel()
{
}

RawModel::RawModel(unsigned int num_buffers, std::initializer_list<VertexBuffer*> args)
{
	this->m_vertex_array_ = VertexArray(num_buffers, args);
}

void RawModel::draw()
{
	m_vertex_array_.draw();
}
