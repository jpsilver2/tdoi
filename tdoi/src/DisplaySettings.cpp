#include "../include/DisplaySettings.h"

DisplaySettings::DisplaySettings(){
    //Default constructor
}

DisplaySettings::DisplaySettings(std::string filename){
    std::ifstream settings_file;
    std::ofstream settings_file_new;
    settings_file.open(filename.c_str());
    if(!settings_file){
        settings_file_new.open(filename.c_str());
        std::cout << "Invalid settings file." << std::endl;
        settings_file_new << "WIDTH:\n" << 1024;
        settings_file_new << "HEIGHT:\n" << 768;
        settings_file_new << "SAMPLES:\n" << 4;
        settings_file_new << "GL_MAJOR:\n" << 3;
        settings_file_new << "GL_MINOR:\n" << 3;
        settings_file_new << "FORWARD_COMPATABLE:\n" << true;
        settings_file_new << "TITLE:\n" << "The Definition of Insanity";
        settings_file_new.close();
    }
    settings_file.close();
    settings_file.open(filename.c_str());
    std::string line;
    while(std::getline(settings_file, line)){
        if(line.compare("WIDTH:") == 0){
            settings_file >> this->m_width;
        }else
        if(line.compare("HEIGHT:") == 0){
            settings_file >> this->m_height;
        }else
        if(line.compare("SAMPLES:") == 0){
            settings_file >> this->m_samples;
        }else
        if(line.compare("GL_MAJOR:") == 0){
            settings_file >> this->m_gl_major;
        }
        else
        if(line.compare("GL_MINOR:") == 0){
            settings_file >> this->m_gl_minor;
        }else
        if(line.compare("FORWARD_COMPATABLE:") == 0){
            settings_file >> this->forward_compatable;
        }
        else
        if(line.compare("TITLE:") == 0){
            std::getline(settings_file, this->m_title);
        }
    }
}

DisplaySettings::DisplaySettings(unsigned int width, unsigned int height, unsigned int samples,
                        unsigned int gl_major, unsigned int gl_minor, bool is_forward_compatable,
                        std::string title)
{
    this->m_width = width;
    this->m_height = height;
    this->m_samples = samples;
    this->m_gl_major = gl_major;
    this->m_gl_minor = gl_minor;
    this->m_title = title;
}

unsigned int DisplaySettings::getWidth(){
    return this->m_width;
}

unsigned int DisplaySettings::getSamples(){
    return this->m_samples;
}

unsigned int DisplaySettings::getHeight(){
    return this->m_height;
}

unsigned int DisplaySettings::getGLMajor(){
    return this->m_gl_major;
}

unsigned int DisplaySettings::getGLMinor(){
    return this->m_gl_minor;
}

bool DisplaySettings::isForwardCompatable(){
    return this->forward_compatable;
}

std::string DisplaySettings::getTitle(){
    return this->m_title;
}
