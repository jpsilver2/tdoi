#include "../include/Material.h"

Material::Material()
{
    //ctor
}

Material::Material(unsigned char surface_type, Program* shader_program, Texture* texture){
    this->m_surfaceType = surface_type;
    this->m_pShader = shader_program;
    this->m_pTexture = texture;
}

void Material::useMaterial(glm::mat4 mvp){
    this->m_pShader->useProgram(mvp);
    this->m_pTexture->bind();
}

Program* Material::getShaderProgram(){
    return this->m_pShader;
}

Texture* Material::getTexture(){
    return this->m_pTexture;
}

void Material::Delete(){
    this->m_deleted = true;
}

bool Material::isDeleted(){
    return this->m_deleted;
}

void Material::setTexture(Texture* texture, bool delete_old){
    if(delete_old){
        this->m_pTexture->Delete();
    }
    this->m_pTexture = texture;
}

unsigned char Material::getSurfaceType(){
    return this->m_surfaceType;
}

Material::~Material()
{
    //dtor
}

void Material::setSurfaceType(unsigned char surface_type)
{
}

void Material::setShaderProgram(Program * program, bool delete_old)
{
}
