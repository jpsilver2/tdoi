// tdoi.cpp : Defines the entry point for the console application.
#include <iostream>
#include "include\Display.h"

int main()
{
	DisplaySettings settings("./Settings.data");
	Display display(settings);
	if (display.hasError()) {
		std::cout << "Error initializing opengl!" << std::endl;
	}
	display.gameLoop();
    return 0;
}

